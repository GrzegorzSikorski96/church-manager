﻿using Microsoft.AspNetCore.Identity;

namespace ChurchManagerBackend.Data.Tables
{
    public class ApplicationUser : IdentityUser
    {
    }
}
